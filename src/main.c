#include <device.h>
#include <sys/printk.h>
#include <drivers/uart.h>
#include <drivers/gpio.h>
#include <zephyr.h>
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h> 
#include <stm32l476xx.h>


struct uart_config uart_cfg_check;
volatile uint8_t answer[5];
volatile bool buffer_recieved=false;
volatile uint8_t buffer[30];
volatile uint8_t temp, step=0;
volatile bool cr = false;
const struct uart_config uart_cfg = {
		.baudrate = 9600,
		.parity = UART_CFG_PARITY_NONE,
		.stop_bits = UART_CFG_STOP_BITS_1,
		.data_bits = UART_CFG_DATA_BITS_8,
		.flow_ctrl = UART_CFG_FLOW_CTRL_NONE
		
};

#define myuart_UART "UART_1"
static void myuart_send(struct device *uart);
static void myuart_recieve(struct device *uart);
void solution();

#define STACKSIZE 2048
static struct gpio_callback pin_cb_data;
struct device *gpio_dev;
struct device *uart;
void impulse(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
int frequancy(int time);
volatile int answer_LED=0, first=1,wave1=1; 
volatile int Send_ticks=0;
volatile float period,period_check,fr_global;
volatile float tim_clock=83333333.33;
volatile bool reading = false;
u32_t start_time_impulse, start_time_send, check;


void main(void)
{
    
    gpio_dev = device_get_binding("GPIOC");
    gpio_pin_configure(gpio_dev, 2, GPIO_ACTIVE_HIGH | GPIO_INPUT | GPIO_INT_EDGE_BOTH); // input
    gpio_pin_configure(gpio_dev, 3, GPIO_ACTIVE_HIGH | GPIO_OUTPUT) ; // output
    gpio_init_callback(&pin_cb_data, impulse , BIT(2)); // nusako nuo kurio "pin" ieiti i "interupt" funkcija 
	gpio_add_callback(gpio_dev, &pin_cb_data); // aktyvuoja "interupt" 
    uart = device_get_binding(myuart_UART);
	uart_configure(uart, &uart_cfg);
	uart_irq_callback_set(uart, myuart_recieve);
	uart_irq_rx_enable(uart);
    


}
void LED(void)
{
    volatile int final_period=0, time=0;
    volatile int delta=0,delay=0;
    s64_t time_stamp;
    s64_t milliseconds_spent;
    while(1)
    {
        //printk("test1 ");
        if (answer_LED==1)
        {
        period_check=period;
        printk("Wall %" PRIu32 "\n", time+1);
        final_period=frequancy(time);
        
        Send_ticks=k_cycle_get_32();
        time_stamp = k_uptime_get();
        while (1)
        {
            milliseconds_spent=k_uptime_get() - time_stamp;
          start_time_send=k_cycle_get_32() - Send_ticks;
          if(start_time_send>=final_period)
          {
            gpio_pin_toggle(gpio_dev, 3);
            gpio_pin_toggle(gpio_dev, 3);
            Send_ticks=k_cycle_get_32();
            delay++;
          }
          delta=fr_global-floor((1/((period*2)/tim_clock)));
          delta=abs(delta);
          if(delta>3 && delay>10)
          {
            printk("Pass \n");
            printk("Delta %" PRIu32 "\n", delta);
            break;
          }
          if(milliseconds_spent>10000)
          {
            printk("Time up \n"); 
            
            break; 
          }
        }
        time++;
        if(time>=6) time=0;
        answer_LED=0;
        k_msleep(100);
        }
    k_msleep(10);
    }      
}
void impulse(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
    if(wave1==0)
    {
        if(first==1)
        {
          first=0;
          start_time_impulse = k_cycle_get_32();
        }
        else
        {
          first=1;
          answer_LED=1;
          period = k_cycle_get_32() - start_time_impulse;
        }
      }
  wave1=0;
}
int frequancy(int time)
{
  volatile double final_period;
  volatile double fr_period,fr;
  volatile int fr_int, fr_period_int;
  fr_period=(period_check*2);
  fr_period_int=fr_period;
  fr=floor(1.0/(fr_period/(tim_clock)));
  fr_global=fr;
  fr_int=fr;
  printk("Frequency %" PRIu32 "\n", fr_int);

  switch(time)
  {
    case 0: 
    final_period=fr_period;
    break;
    case 1: 
    final_period=round((1/(fr+50))*tim_clock);
    break;
    case 2: 
    final_period=round((1/pow(fr,1.05))*tim_clock);
    break;
    case 3:
    final_period=round(1/(fr/(3.14159265359/2))*tim_clock);
    break;
    case 4:
    fr_int=fr_int>>1;
    fr=fr_int;
    final_period=round((1/fr)*tim_clock);
    break;
    case 5:
    final_period=round((1/(fr/(1.61803398875/2)))*tim_clock);
    break;
  }
  return final_period;
}
void UART(void) {
	while(1)
	{
         //printk("test2 ");
		if (buffer_recieved)
		{
			solution();
			myuart_send(uart);
			buffer_recieved=false;
			memset(answer, '\0', sizeof(answer)); // cleans answer buffer 
    	    memset(buffer, '\0', sizeof(buffer));
            k_msleep(100);
		}
        if(!reading) k_msleep(10);  
	}
}
static void myuart_send(struct device *uart) {
    int i;
   volatile uint8_t temp;
		i=sizeof(answer)-1;
  		while (i>=0) {
		temp=answer[i];
		if (temp=='\0') //ship empty spaceses 
        {
            i--;
            continue;
        }
		uart_poll_out(uart, (char*)temp);
		printk("%c", temp);
        i--;
    }
}
static void myuart_recieve(struct device *uart)
{
	if (uart_irq_rx_ready(uart))
	{
        reading=true;
		uart_poll_in(uart, &temp);
		if ((temp == 0x0d) || (cr==true)) //CR
        {
            cr=true;
            if (temp == 0x0a) // LF
            {
                buffer_recieved = true;
                reading = false;
                step = 0;
                cr=false;
            } 
        }
        else
        {
            buffer[step] = temp;
            step++;
        }
	}
    

}
void solution()
{
    int sk_flag=0, s=0, f=0, flag=0, ne_flag=0, div_flag=0, is_x=0, clean=0, first_x=1;
    volatile uint8_t int_buffer[10];
    volatile float int_arry[10], dis;
    volatile int half_answer;
    memset(int_arry,0,sizeof(int_arry));
        for(int j=0; j<sizeof(buffer); j++)
        {
            if(buffer[j] == '-'|| buffer[j] == '+'|| buffer[j] == '*'|| buffer[j] == '/' || buffer[j] == 'x'||buffer[j] == '='||buffer[j] == '('||buffer[j] == ')'||buffer[j] == '^')
            {   
                if (buffer[j] == '(') flag=1; //checks if equation has('x^2')
                if (buffer[j] == '/') div_flag=1; // division flag
                if(j==s && buffer[j+1]=='(')
                {
                    f++;
                    is_x=1;

                }
                if (sk_flag==1) // fuse saved numbers into one from buffer 
                {
                    if (flag==1 && clean==0)
                    {
                        clean=1;
                        sk_flag=0;
                        s=0;
                        continue;
                    }
                    else if (ne_flag==1 && div_flag==0)
                    {
                        int_arry[f] -= atoi((char *)int_buffer);
                        
                    }
                    else if (div_flag==1 && ne_flag==0)
                    {
                        int_arry[0] += 1.0/atoi((char *)int_buffer);
                        is_x=0;
                        f--;
                    }
                    else if (div_flag==1 && ne_flag==1)
                    {
                        int_arry[0] -= 1.0/atoi((char *)int_buffer);
                        is_x=0;
                        f--;
                    }
                    else int_arry[f] = atoi((char *)int_buffer);
                    
                    memset(int_buffer, '\0', sizeof(int_buffer)); //clears buffer for new calculations
                    sk_flag=0;
                    ne_flag=0;
                    div_flag=0;
                    s=0;
                    f++;
                }
                if (buffer[j] == '-' )
                {
                ne_flag=1; //negative number
                } 
                 if (buffer[0]=='x' && first_x==1)
                 {
                     f++;
                     first_x=0;
                 }
            }
            else 
            {
                sk_flag=1; // marks that readers just read a digit
                int_buffer[s]=buffer[j];
                s++;
            } 
            if(buffer[j] == '\0') break; // checks if it's the end of equation 
            
        }
        if (flag==0) //'x' equations are sloved here
        {
            half_answer=abs(round(int_arry[1]/int_arry[0]));
        }
        if (flag==1) //'x^2' equations are sloved here
        {
            
            if (is_x==1)
            {
                 half_answer=round(sqrt(abs(round(int_arry[2]/int_arry[1]))));

            }
            else
            {
                dis=pow(int_arry[0],2)-4*int_arry[1]*int_arry[2];//calculating discriminat
                half_answer=round((sqrt(dis)-int_arry[0])/(2*int_arry[1]));// calculating final answer

            }
        }
        int i=0;
        while(half_answer!=0) // converts decimal number to hex ASCII
        {
            answer[i] = 0x30|half_answer%10; //ASCII conversion
            half_answer=half_answer/10;
            i++;
        }
}
K_THREAD_DEFINE(UART_id, STACKSIZE, UART, NULL, NULL, NULL,
		1, 0, 0);
K_THREAD_DEFINE(LED_id, STACKSIZE, LED, NULL, NULL, NULL,
		1, 0, 0);
